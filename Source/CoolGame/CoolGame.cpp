// Copyright 1998-2019 Epic Games, Inc. All Rights Reserved.

#include "CoolGame.h"
#include "Modules/ModuleManager.h"

IMPLEMENT_PRIMARY_GAME_MODULE( FDefaultGameModuleImpl, CoolGame, "CoolGame" );

DEFINE_LOG_CATEGORY(LogCoolGame)
 