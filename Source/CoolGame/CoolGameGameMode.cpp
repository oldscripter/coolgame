// Copyright 1998-2019 Epic Games, Inc. All Rights Reserved.

#include "CoolGameGameMode.h"
#include "CoolGamePlayerController.h"
#include "CoolGameCharacter.h"
#include "UObject/ConstructorHelpers.h"

ACoolGameGameMode::ACoolGameGameMode()
{
	// use our custom PlayerController class
	PlayerControllerClass = ACoolGamePlayerController::StaticClass();

	// set default pawn class to our Blueprinted character
	static ConstructorHelpers::FClassFinder<APawn> PlayerPawnBPClass(TEXT("/Game/TopDownCPP/Blueprints/TopDownCharacter"));
	if (PlayerPawnBPClass.Class != NULL)
	{
		DefaultPawnClass = PlayerPawnBPClass.Class;
	}
}