// Copyright 1998-2019 Epic Games, Inc. All Rights Reserved.

#pragma once

#include "CoreMinimal.h"
#include "GameFramework/GameModeBase.h"
#include "CoolGameGameMode.generated.h"

UCLASS(minimalapi)
class ACoolGameGameMode : public AGameModeBase
{
	GENERATED_BODY()

public:
	ACoolGameGameMode();
};



